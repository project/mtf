If you have ever had a use case that called for a double or triple text field
with those text fields grouped together, this module may be for you

However, in many cases https://drupal.org/project/field_collection would
be a better choice due to its flexibility.

With Field Collection you can redefine which text fields are associated,
but with  Multi Text Fields they are permanently glued together.

So why this module? It provides a slightly easier setup for having multiple text 
fields in one field, and can make getting the data out easier in some cases.
That also includes Views; this module comes with a Views intergation. 
Keep in mind this module only allows for text fields.

Features

- 1-3 text fields grouped into a single field.
- Field formatters to display only text box 1 (Title), text box 2 (Details), 
or text box 3 (Additional)
-Views integration

Installation and Usage

- Install and enable module either from UI or with Drush
- Go to a content type and add a field; choose "Multi Text Field". 
If you do not see this, clear your cache at admin/config/development/performance
since Drush might not have cleared all caches (for example APC, Memcache etc).
-It will ask whether you want two text fields or three.
- Customize the text labels and save your field

To do

- Views integration
